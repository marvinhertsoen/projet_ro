# Author : SV
# date 2015/10/12

############################
# Comparaison d'algorithmes

# lit les donnÃ©es du HC best-improvement
ils50 <- read.table("/home/marvin/NetBeansProjects/projet_ro/data/data_ils50.csv", header = TRUE, sep = ",")
ils25 <- read.table("/home/marvin/NetBeansProjects/projet_ro/data/data_ils25.csv", header = TRUE, sep = ",")
ils10 <- read.table("/home/marvin/NetBeansProjects/projet_ro/data/data_ils10.csv", header = TRUE, sep = ",")
hcfi  <- read.table("/home/marvin/NetBeansProjects/projet_ro/data/hcfi_out.csv", header = TRUE, sep = ",")

hist(ils50[ils50$nbeval == 50,]$fitness, main="Histogramme 50 ILS", xlab="Fitness", ylab="Quantité")
hist(ils25[ils25$nbeval == 25,]$fitness, main="Histogramme 25 ILS", xlab="Fitness", ylab="Quantité")
hist(ils10[ils10$nbeval == 10,]$fitness, main="Histogramme 10 ILS", xlab="Fitness", ylab="Quantité")
hist(hcfi[hcfi$nbeval == 600000,]$fitness, main="Histogramme HCFI", xlab="Fitness", ylab="Quantité")


#plot(bi$fitness, type = "l")
moy50<- mean(ils50$fitness)
moy25<- mean(ils25$fitness)
moy10<- mean(ils10$fitness)
moyhcfi <-mean(hcfi$fitness)

maxfit50 <- max(ils50$fitness)
maxfit25 <- max(ils25$fitness)
maxfit10 <- max(ils10$fitness)
maxfithcfi <-max(hcfi$fitness)

var50 <- var(ils50$fitness)
var25 <- var(ils25$fitness)
var10 <- var(ils10$fitness)
varhcfi <- var(hcfi$fitness)

#ecart type
sd50 <- sd(ils50$fitness)
sd25 <- sd(ils25$fitness)
sd10 <- sd(ils10$fitness)
sdhcfi <-sd(hcfi$fitness)

#norm
hx50 <- dnorm(ils50$fitness)
hx25 <- dnorm(ils250$fitness)
hx10 <- dnorm(ils10$fitness)
hxhcfi <-dnorm(hcfi$fitness)

x_fitness <- seq(9,20, by = 0.01)
plot (x_fitness, dnorm(x_fitness, mean = moy50, sd = sd50), type = "l", col="red", main="Distribution des Fitness", xlab="Fitness", ylab="")
lines (x_fitness, dnorm(x_fitness, mean = moy25, sd = sd25), type = "l", col="blue")
lines (x_fitness, dnorm(x_fitness, mean = moy10, sd = sd10), type = "l", col="green")

summary(ils50[ils50$nbeval == 50,]$fitness)
summary(ils25[ils25$nbeval == 25,]$fitness)
summary(ils10[ils10$nbeval == 10,]$fitness)


plot(ils50$fitness, type="l", main="Evolution de l'ILS 50", xlab="Itérations", ylab="Fitness")
plot(ils25$fitness, type="l", main="Evolution de l'ILS 25", xlab="Itérations", ylab="Fitness")
plot(ils25$fitness, type="l", main="Evolution de l'ILS 10", xlab="Itérations", ylab="Fitness")

x_fitness <- seq(5,20, by = 0.01)
plot (x_fitness, dnorm(x_fitness, mean = moy50, sd = sd50), type = "l", col="red", main="HCFI vs ILS", xlab="Fitness", ylab="")
lines (x_fitness, dnorm(x_fitness, mean = moyhcfi, sd = sdhcfi), type = "l", col="orange")