data/ contient les résultats des algo en version csv
R/ contient le script r pour la génération des charts

Lancer 50 itérations de l'ILS avec output path 'ils_out.csv'
(Pour info, chaque itération ILS est composée de 600000 HCFI)
> java -jar dist/projet_ro.jar -n 50 -t "ILS"  -o "ils_out.csv"


Lancer 600000 itérations d'HCFI avec output path 'hcfi_out.csv' : 
> java -jar dist/projet_ro.jar -n 600000 -t "HCFI"  -o "hcfi_out.csv"

Lancer 600000 itérations du RECUIT SIMULE avec output path 'rs_out.csv' : 
> java -jar dist/projet_ro.jar -n 600000 -t "RS"  -o "rs_out.csv"