/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_ro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author herts
 */
public class Filemanager {
    
    //String fileName = "ks_5.dat";
    String line = null;
    String read = "";
    
    int nb_items = 0;
    int contrainte = 0 ;
    List<String> w = new ArrayList();
    List<String> p = new ArrayList<String>();
    
     Filemanager(String file_output){
        if(!fileExists(file_output)){ 
            try {
                    FileWriter writer = new FileWriter(file_output, true);
                    CSVUtils.writeLine(writer, Arrays.asList("nbeval","fitness","rules"));
                    writer.flush(); writer.close();
                }
                catch(FileNotFoundException ex) {System.out.println("Unable to open file '" + file_output + "'");}
                catch(IOException ex) {System.out.println("Error reading file '"+ file_output + "'");}
        }
    }
        
    public String readFile(String filename){
        
        try {
            
           FileReader fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            int i=0;
            while((line = bufferedReader.readLine()) != null) {
                if(i==0){
                    nb_items = Integer.parseInt(line);
                }
                else if(i==1){
                    p = Arrays.asList(line.split(" "));
                }
                else if(i==2){
                    w = Arrays.asList(line.split(" "));
                }
                else if(i==3){
                    contrainte = Integer.parseInt(line);
                }
                i++;
            }
            bufferedReader.close();   
        }
        catch(FileNotFoundException ex) {System.out.println("Unable to open file '" + filename + "'");}
        catch(IOException ex) {System.out.println("Error reading file '"+ filename + "'");}
        return read;
    }
    
    public void writeFile(String filename, int nbEval, Solution bestSol, int[]rules){
        
        try {
            System.out.println(Arrays.toString(rules));
            FileWriter writer = new FileWriter(filename, true);
            
            String r = Arrays.toString(rules);
            r = r.replaceAll(", ", " ").replace("[", "").replace("]", "");
                        
            CSVUtils.writeLine(writer, Arrays.asList(Integer.toString(nbEval),Double.toString(bestSol.fitness), r));
            writer.flush();
            writer.close();
        }
        catch(FileNotFoundException ex) {System.out.println("Unable to open file '" + filename + "'");}
        catch(IOException ex) {System.out.println("Error reading file '"+ filename + "'");}

    }

    public boolean fileExists(String filename){
        boolean exists = false;
        File f = new File(filename);
        if(f.exists() && !f.isDirectory()) { 
            exists = true;
        }
        return exists;
    }
}
