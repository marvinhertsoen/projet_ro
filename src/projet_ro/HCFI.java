/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_ro;

/**
 *
 * @author marvin
 */
public class HCFI {
    public  Solution    bestSol;
    public  Solution    currentSol;

    HCFI(){
        bestSol = new Solution(); bestSol.init();
        currentSol = new Solution(); currentSol.init();
    }
    public void run(int nb_iter, int nb_neighbors){
     
        int optimum_while, optimum_for;
        for (int i=0;i<nb_iter;i++){

            bestSol.getNeighbor(currentSol, nb_neighbors);
            currentSol.Eval();


                    
            if (currentSol.fitness > bestSol.fitness) System.out.println("Better Found : " +currentSol.fitness + " ; i= " +i /*+" good:"+prct_better+" bof:"+prct_equal+" bad:"+prct_lower*/);            
                // Test
            if(currentSol.betterThan(bestSol)){
                bestSol.rules = currentSol.rules;
                bestSol.Eval();
                
            }
            else if(currentSol.equalsOrBetterThan(bestSol)){
                bestSol.rules = currentSol.rules;
                bestSol.Eval();
            }
        }
    }
    
    public void mutate(int nb_mut){
        bestSol.getNeighbor(bestSol, nb_mut);
        bestSol.Eval();
    }
}
