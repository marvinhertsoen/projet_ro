/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_ro;

import java.util.Random;

/**
 *
 * @author marvin
 */
public class ILS {
    public  Solution    bestSol;
    public  HCFI        hill_climber;
    ILS(){
        bestSol = new Solution(); bestSol.init();
        hill_climber = new HCFI();
    }
    public void run(int nb_iter){
        Random r = new Random();
        int perturb = 0;
        for(int i=0; i<nb_iter; i++){
            hill_climber.run(600000,1);

            if(hill_climber.bestSol.betterThan(bestSol)){
                bestSol.rules = hill_climber.bestSol.rules;
                bestSol.fitness = hill_climber.bestSol.fitness;
            }
            //Display
            System.out.print("BestSol : "+bestSol.fitness+" ==> "); bestSol.displayRules();

            // Mutation
            hill_climber.bestSol = new Solution(bestSol);

            perturb = r.nextInt(4-2)+2; // entre 2 & 3
            hill_climber.mutate(perturb);
            System.out.println("mutation..."+perturb);
        }
        System.out.println();
        System.out.println("Best Score ILS: " +bestSol.fitness);

    }
}
