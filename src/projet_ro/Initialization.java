/**
 * 
 */
package projet_ro;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author verel
 *
 */
public class Initialization {

	int nbRules = 96;
	
	Random generator;
        List<Integer> relevant_indexes;
	
	public Initialization() {
		generator = new Random();
                
                relevant_indexes = new ArrayList<Integer>();
                    /*gcd*/
                for(int g=0;g<4;g++){
                    for(int c=0;c<4;c++){
                        for(int d=0;d<4;d++){
                            if (!(g==0 && c== 0 && d == 0) && !(g==1 && c== 1 && d == 1)&& !(g==1 && c== 0 && d == 0)&& !(g==2 && c== 0 && d == 0)){
                                relevant_indexes.add(g * 36 + c * 6 + d);
                            }
                        }
                    }
                }
                
                    /*gcb*/
                for(int g=0;g<4;g++){
                    for(int c=0;c<4;c++){
                        if (!(g== 0 && c == 0) && !(g==1 && c== 1) && !(g==1 && c== 0) && !(g==2 && c== 0)){
                            relevant_indexes.add(g * 36 + c * 6 + 5); 
                        }
                    }
                }
                    /*bcd*/
                for(int c=0;c<4;c++){
                    for(int d=0;d<4;d++){
                        if (! (c== 0 && d == 0) && !(c==1 && d== 1) && !(c==1 && d == 0) ){
                            relevant_indexes.add(5 * 36 + c * 6 + d); 
                        }
                    }
                }  
	}
	
	public void init(int [] rules) {
                
            //System.out.println(relevant_indexes.size());
           // System.out.println(relevant_indexes);

           //System.out.print(relevant_indexes.size() + " : ");
            for(int i = 0; i < relevant_indexes.size(); i++){
                //System.out.print(relevant_indexes.get(i) + " ");
                rules[ relevant_indexes.get(i)] = generator.nextInt(4);
            }//System.out.println();*/
                
            /*
            0 180 5 43 187 47 36 72 41 77 186
            */
            // les regles repos (obligatoires)
            setRule(rules, 0, 0, 0, 0);
            setRule(rules, 5, 0, 0, 0);
            setRule(rules, 0, 0, 5, 0);

            // les regles feu (trés conseillés)
            setRule(rules, 1, 1, 1, 4);
            setRule(rules, 5, 1, 1, 4);
            setRule(rules, 1, 1, 5, 4);

            // les regles a priori (signal de période 2 vers la droite)
            setRule(rules, 1, 0, 0, 2);
            setRule(rules, 2, 0, 0, 1);
           
            // a priori bord droit
            setRule(rules, 1, 0, 5, 1); // pour taille 2
            setRule(rules, 2, 0, 5, 2);                     // PB
            
            // a priori bord gauche (pour la taille 2)
            setRule(rules, 5, 1, 0, 1);

	}
    /*
     * Ecrit la regle 
     *
     * g : etat de la cellule de gauche
     * c : etat de la cellule centrale
     * d : etat de la cellule de droite
     * r : etat de la cellule centale à t+1
     */
    public void setRule(int [] rules, int g, int c, int d, int r) {
	rules[g * 36 + c * 6 + d] = r;
        //System.out.println(g * 36 + c * 6 + d);
    }
    //System.out.println(rules);
}
