/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_ro;

import java.util.Random;

/**
 *
 * @author marvin
 */
public class RS {
    
    public  double      temp;
    public  double      cooling;
    public  double      delta;
    public  Solution    bestSol;
    public  Solution    currentSol;
    public  Random      rand;

    RS(){
        bestSol     = new Solution(); bestSol.init();
        currentSol  = new Solution(); currentSol.init();
        temp        = 0.5;
        cooling     = 0.99;
        delta       = 0;
        rand = new Random();
    }
    public void run(int nb_iter, int nb_neighbors){
     
       int optimum_while, optimum_for;
       int u=0;
       int count=0;
       for (int i=0;i<nb_iter;i++){

            bestSol.getNeighbor(currentSol, nb_neighbors); 
            currentSol.Eval();
            delta = currentSol.fitness - bestSol.fitness;
            
            if(delta > 0){ System.out.println("Best fitness " +bestSol.fitness); }
            if(delta >= 0){
                bestSol.rules = currentSol.rules;
                bestSol.Eval();
            }
            else{
                u = rand.nextInt(2); //verifié ! :) 
                if(u<Math.exp(delta/temp)){
                    bestSol.rules = currentSol.rules;
                    bestSol.Eval();
                }
            }
            count++;
            if(count > 100*85){
                count=0;
                temp *= cooling;
            }
        }
    }
    
    public void mutate(int nb_mut){
        bestSol.getNeighbor(bestSol, nb_mut);
        bestSol.Eval();
    }
}
