/**
 * 
 */
package projet_ro;
import java.io.*;


/**
 * @author verel
 *
 */
public class Run {

    /**
     * @param args
     */
    public static void main(String[] args) {
        
        int nb_evaluation = 100000000;
        String type = "RS"; //0:HCFI 1:ILS 2:KS
        String file_stats_output = "./output.csv";

        if(args.length > 1){
            if(args[0].equals("-n")){
                nb_evaluation = Integer.parseInt(args[1]);
            } 
        }
        if(args.length  >3){
            //if(args[2].equals("-i")){ file_input = args[3]; } 
            if(args[2].equals("-t")){ type = args[3]; } 
        }
        if(args.length  >5){
            if(args[4].equals("-o")){
                file_stats_output = args[5];
            } 
        }
            
	Automata automate = new Automata(20);
        
	// path to change
	String path = "/home/marvin/NetBeansProjects/projet_ro/";

	//String bestName = path + solFileName + ".dat";
	int [] rules = new int[216]; //initRulesFromFile(bestName);
        Initialization init = new Initialization();
                
	// Nombre maximale de fusiliers (taille maximale du réseau)
	int sizeMax = 20;
	int nFire;

        Solution bestSol = new Solution();
        if(type.equals("ILS")){
            ILS iterated_ls = new ILS();
            iterated_ls.run(80);
            bestSol = new Solution(iterated_ls.bestSol);
        }
        else if(type.equals("HCFI")){
            HCFI hill_climber = new HCFI();
            hill_climber.run(nb_evaluation,1);
            bestSol = new Solution(hill_climber.bestSol);
        }
        else if(type.equals("RS")){
            RS recuit_simu = new RS();
            recuit_simu.run(nb_evaluation,1);
            bestSol = new Solution(recuit_simu.bestSol);
        }
        
        
        Filemanager fm = new Filemanager(file_stats_output);
        fm.writeFile(file_stats_output,nb_evaluation, bestSol, bestSol.rules);
        
        String outName = path + "out.dat";

        PrintWriter ecrivain;
        try {
	    ecrivain =  new PrintWriter(new BufferedWriter(new FileWriter(outName)));
            saveSolInFile(bestSol.getfitness(), bestSol.rules, ecrivain);
	    ecrivain.close();
	}catch (Exception e){ System.out.println(e.toString()); }
        
	for(int i = 2; i <= sizeMax; i++) {  // évolution de l'automate avec la règle rule sur un réseau de longueur i
            
	    // évolution de l'automate avec la règle rule sur un réseau de longueur i
	    nFire = automate.evol(bestSol.rules, i);

	    // affichage du nombre de fusiliers ayant tiré
	    System.out.println("longueur " + i + " : " + nFire);
	    
	    // affiche la dynamique dans un fichier au format svg
	   // automate.exportSVG(i, 2 * i - 2, path + "svg/" + solFileName + "_" + i + ".svg");
            automate.exportSVG(i, 2 * i - 2, path + "svg/" + "test" + "_" + i + ".svg");
	} 
        
        bestSol.displayRules();
	System.out.println("The End.");
        
    }

    public static void saveSolInFile(int fitness, int [] rules, PrintWriter ecrivain) {
	ecrivain.print(fitness);
	for(int i = 0; i < 216; i++) {
	    ecrivain.print(" ");
	    ecrivain.print(rules[i]);
	}
	ecrivain.println();
    }
    

    public static int [] initRulesFromFile(String fileName) {
	// 5 états + l'état "bord"
	int n = 5 + 1;

	int [] rules = new int[n * n * n];

	try {
	    FileReader fichier = new FileReader(fileName);

	    StreamTokenizer entree = new StreamTokenizer(fichier);

	    int i = 0;
	    while(entree.nextToken() == StreamTokenizer.TT_NUMBER)
		{		
		    rules[i] = (int) entree.nval;
		    i++;
		} 
	    fichier.close();
	}		
	catch (Exception e){
	    System.out.println(e.toString());
	}

	return rules;
    }
}
