/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_ro;

/**
 *
 * @author marvin
 */
public class Solution {
    Automata automate;

    int[] rules;
    int fitness =0;
    Initialization init;
    
    public Solution(){
        this.init = new Initialization();
        this.automate = new Automata(30);
        this.rules   = new int[216];
    }
    
    public Solution(Solution s){
        this.init = new Initialization();
        this.automate = new Automata(30);
        this.rules   = s.rules.clone();
        this.fitness   = s.fitness;
    }
    
    public void init(){   
        this.init.init(this.rules);
    }
    
    public int Eval(){
        this.fitness = automate.f(this.rules, 30);
        return this.fitness;
    }
    
   public int[] getNeighbor(Solution sol_to_update, int nb_neighbors){
        sol_to_update.rules = this.rules.clone();
        int rand_index_relevant, rand_rules_index;
        int newval;
        int val_temp;
        int[] test = new int[nb_neighbors];
        for (int i=0; i < nb_neighbors; i++){
            rand_index_relevant = init.generator.nextInt(init.relevant_indexes.size()); // index entre 0-85
            rand_rules_index    = init.relevant_indexes.get(rand_index_relevant); // l'index aleat pour le tab rules
            val_temp = sol_to_update.rules[rand_rules_index];
            
            newval = init.generator.nextInt(4);
            if(newval == val_temp){ newval = newval+1%4; }
            sol_to_update.rules[rand_rules_index] = newval;
            test[i] = rand_rules_index;
        }
        return test;
    }
   
    public boolean betterThan(Solution s){
        if(s.getfitness() < this.getfitness()) return true;
        else                                   return false;
    }
    public boolean equalsOrBetterThan(Solution s){
        if(s.getfitness() <= this.getfitness()) return true;
        else                                   return false;
    }
   
    public int getfitness(){
        return this.fitness;
    }
    
    public void displayRules(){
        for(int i=0; i<this.rules.length; i++){
            System.out.print(this.rules[i] + ", ");
        }
        System.out.println();
    }
}
